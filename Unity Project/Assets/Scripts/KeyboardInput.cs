﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardInput : MonoBehaviour {

    public class CharacterKeyPressedEvent : UnityEvent<char>{}

	public class NumericalKeyPressedEvent : UnityEvent<int>{}

    private const int ASCII_NUMERICAL_0 = 48;
    private const int ASCII_NUMERICAL_9 = 57;
    private const int ASCII_LOWERCASE_A = 97;
    private const int ASCII_LOWERCASE_Z = 122;

    private static CharacterKeyPressedEvent CharacterKeyPressed = new CharacterKeyPressedEvent();
	private static NumericalKeyPressedEvent NumericalKeyPressed = new NumericalKeyPressedEvent();
    private static UnityEvent EnterKeyPressed = new UnityEvent();
	private static UnityEvent SpaceKeyPressed = new UnityEvent();
    private static UnityEvent BackspaceKeyPressed = new UnityEvent();
    private static UnityEvent EscapeKeyPressed = new UnityEvent();

    public static void AddCharacterKeyPressedListener(UnityAction<char> callback)
    {
        CharacterKeyPressed.AddListener(callback);
    }

	public static void AddNumericalKeyPressedListener(UnityAction<int> callback)
	{
		NumericalKeyPressed.AddListener(callback);
	}

    public static void AddEnterKeyPressedListener(UnityAction callback)
    {
        EnterKeyPressed.AddListener(callback);
    }

	public static void AddSpaceKeyPressedListener(UnityAction callback)
	{
		SpaceKeyPressed.AddListener(callback);
	}

    public static void AddBackspaceKeyPressedListener(UnityAction callback)
    {
        BackspaceKeyPressed.AddListener(callback);
    }

    public static void AddEscapeKeyPressedListener(UnityAction callback)
    {
        EscapeKeyPressed.AddListener(callback);
    }

    public static void RemoveCharacterKeyPressedListener(UnityAction<char> callback)
    {
        CharacterKeyPressed.RemoveListener(callback);
    }

    public static void RemoveSpaceKeyPressedListener(UnityAction callback)
    {
        SpaceKeyPressed.RemoveListener(callback);
    }

    public static void RemoveBackspaceKeyPressedListener(UnityAction callback)
    {
        BackspaceKeyPressed.RemoveListener(callback);
    }

    public static void RemoveEscapeKeyPressedListener(UnityAction callback)
    {
        EscapeKeyPressed.RemoveListener(callback);
    }

	private void Update()
    {
        for(int keyIndex = ASCII_LOWERCASE_A; keyIndex <= ASCII_LOWERCASE_Z; keyIndex++)
        {
            CheckForAlphabeticalKeyPress(keyIndex);
        }

        for (int keyIndex = ASCII_NUMERICAL_0; keyIndex <= ASCII_NUMERICAL_9; keyIndex++)
        {
			CheckForNumericalKeyPress(keyIndex);
        }

        CheckForBackspaceKeyPress();
        CheckForEnterKeyPress();
		CheckForSpaceKeyPress();
        CheckForEscapeKeyPress();
	}

    private void CheckForAlphabeticalKeyPress(int asciiCode)
    {
        char asciiChar = System.Convert.ToChar(asciiCode);

        string key = asciiChar.ToString();

        if (Input.GetKeyDown(key))
        {
            CharacterKeyPressed.Invoke(asciiChar);
        }
    }

	private void CheckForNumericalKeyPress(int asciiCode)
	{
		char asciiChar = System.Convert.ToChar(asciiCode);

		string key = asciiChar.ToString();

		if (Input.GetKeyDown(key))
		{
			int numericalInput = System.Convert.ToInt32(asciiChar);

			NumericalKeyPressed.Invoke((int)(char.GetNumericValue(asciiChar)) - 1);
		}
	}

    private void CheckForBackspaceKeyPress()
    {
        if(Input.GetKeyDown(KeyCode.Backspace))
        {
            BackspaceKeyPressed.Invoke();
        }
    }
    
    private void CheckForEnterKeyPress()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            EnterKeyPressed.Invoke();
        }
    }

	private void CheckForSpaceKeyPress()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			SpaceKeyPressed.Invoke();
		}
	}

    private void CheckForEscapeKeyPress()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeKeyPressed.Invoke();
        }
    }
}
