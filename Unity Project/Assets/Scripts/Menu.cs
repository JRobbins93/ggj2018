﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

	public GameObject MenuObject;

	public GameObject CreditsObject;

	public AudioSource AudioPlayer;

	public void OnCredits()
	{
		MenuObject.SetActive(false);
		CreditsObject.SetActive(true);
	}

	public void OnMenu()
	{
		MenuObject.SetActive(true);
		CreditsObject.SetActive(false);
	}

	public void OnPlay()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("TypingWhitebox");
	}		

	public void OnQuit()
	{
		Application.Quit();
	}

	private void Start()
	{
		DontDestroyOnLoad(AudioPlayer);
	}
}
