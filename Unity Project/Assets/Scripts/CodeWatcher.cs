﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CodeWatcher : MonoBehaviour {

	public class CodeEnteredEvent : UnityEvent<string>{}

	private static CodeEnteredEvent CodeEntered = new CodeEnteredEvent();

	private static Dictionary<string, List<UnityAction<string>>> CodesToWatch = new Dictionary<string, List<UnityAction<string>>>();

	public static void AddCallbackForCode(string code, UnityAction<string> callback)
	{
		code = code.ToLower();

		if (CodesToWatch.ContainsKey(code))
		{
			CodesToWatch[code].Add(callback);
		}
		else
		{
			CodesToWatch.Add(code, new List<UnityAction<string>>{ callback });
		}
	}

	public static void RemoveCallbacksForCode(string code)
	{
		code = code.ToLower();

		CodesToWatch.Remove(code);
	}

	public static void AddCodeEnteredListener(UnityAction<string> callback)
	{
		CodeEntered.AddListener(callback);
	}

	public static void RemoveCodeEnteredListener(UnityAction<string> callback)
	{
		CodeEntered.RemoveListener(callback);
	}

	private void Start()
	{
		CodeEntry.AddCodeUpdatedListener(CheckCode);
	}

	private void CheckCode(string code)
	{
		List<UnityAction<string>> callbacksToTrigger = null;

		if (!CodesToWatch.TryGetValue(code.ToLower(), out callbacksToTrigger))
		{
			return;
		}

		foreach (UnityAction<string> callback in callbacksToTrigger)
		{
			callback(code);
		}

		CodesToWatch.Remove(code);

		CodeEntered.Invoke(code);
	}
}