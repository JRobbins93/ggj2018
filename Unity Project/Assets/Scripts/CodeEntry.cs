﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CodeEntry : MonoBehaviour {

	public class CodeUpdatedEvent : UnityEvent<string>{}

	private static CodeUpdatedEvent CodeUpdated = new CodeUpdatedEvent();

    private Text Readout;

	private StringBuilder StringBuilder = new StringBuilder();

	public static void AddCodeUpdatedListener(UnityAction<string> callback)
	{
		CodeUpdated.AddListener(callback);
	}

	public static void RemoveCodeUpdatedListener(UnityAction<string> callback)
	{
		CodeUpdated.RemoveListener(callback);
	}

    private void Start()
    {
        Readout = GetComponent<Text>();

        KeyboardInput.AddCharacterKeyPressedListener(EnterCharacter);
		KeyboardInput.AddSpaceKeyPressedListener(SpaceCharacter);
        KeyboardInput.AddBackspaceKeyPressedListener(RemoveLastCharacter);
        KeyboardInput.AddEscapeKeyPressedListener(ClearEntry);

		CodeWatcher.AddCodeEnteredListener(ClearEntry);
    }

    private void RefreshReadout()
    {
		Readout.text = StringBuilder.ToString().ToUpper();
    }
    
	private void ClearEntry()
    {
        StringBuilder.Remove(0, StringBuilder.Length);
        RefreshReadout();
    }

	private void ClearEntry(string code)
	{
		ClearEntry();
	}

    private void RemoveLastCharacter()
    {
        if (StringBuilder.Length == 0)
        {
            return;
        }

        StringBuilder.Remove(StringBuilder.Length - 1, 1);

        RefreshReadout();
    }

    private void EnterCharacter(char character)
    {
        StringBuilder.Append(character);

        RefreshReadout();

		CodeUpdated.Invoke(StringBuilder.ToString());
    }

	private void SpaceCharacter()
	{
		EnterCharacter(' ');
	}
}
