﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

	public float StartingDamage;

	public float StartingHealth;

	private float CurrentDamagePerShot;

	private float CurrentMaxHealth;

	private float CurrentHealth;

	private int Level = 0;

	private int ShotsPerAttack = 1;

	public float GetDamagePerShot()
	{
		return CurrentDamagePerShot;
	}

	public int GetShotsPerAttack()
	{
		return ShotsPerAttack;
	}

	public void Repair()
	{
		CurrentHealth = CurrentMaxHealth;
	}

	public void Upgrade()
	{
		CurrentMaxHealth *= 2;
		CurrentDamagePerShot *= 2;
		ShotsPerAttack++;
	}

	private void Awake()
	{
		CurrentDamagePerShot = StartingDamage;
		CurrentHealth = StartingHealth;
		CurrentMaxHealth = CurrentHealth;
	}

	public void ApplyDamage(float damage)
	{
		Debug.LogFormat("{0} damage dealt. Health is {1}", damage, CurrentHealth);
		CurrentHealth -= damage;
	}

	public bool IsDead()
	{
		return CurrentHealth <= 0f;
	}

	public GameObject BarrierMeshObject;

	public float GetHealth()
	{
		return CurrentHealth;
	}
}
