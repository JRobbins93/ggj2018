﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CommandCodeEntry : MonoBehaviour {

	public Text CodeText;

	public UnityEngine.UI.Image ShootIcon;
	public UnityEngine.UI.Image UpgradeIcon;
	public UnityEngine.UI.Image RepairIcon;
	public UnityEngine.UI.Image AirstrikeIcon;

	private RectTransform RectTransform;

	private string Command;

	private string Code;

	public RectTransform GetRectTransform()
	{
		return RectTransform;
	}

	public string GetCommand()
	{
		return Command;
	}

	public string GetCode()
	{
		return Code;
	}

	public void Init(string command, string code)
	{
		Command = command;
		Code = code;

		CodeText.text = code.ToUpper();

		UnityEngine.UI.Image iconToActivate = null;

		ShootIcon.gameObject.SetActive(false);
		UpgradeIcon.gameObject.SetActive(false);
		RepairIcon.gameObject.SetActive(false);
		AirstrikeIcon.gameObject.SetActive(false);

		switch (command.ToLower())
		{
		case "shoot":
			iconToActivate = ShootIcon;
			break;
		case "upgrade":
			iconToActivate = UpgradeIcon;
			break;
		case "repair":
			iconToActivate = RepairIcon;
			break;
		case "airstrike":
			iconToActivate = AirstrikeIcon;
			break;
		}

		iconToActivate.gameObject.SetActive(true);
	}

	private void Awake()
	{
		RectTransform = GetComponent<RectTransform>();
	}
}
