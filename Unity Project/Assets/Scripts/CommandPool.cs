﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPool : MonoBehaviour {

	public enum Command { Shoot, Repair, Upgrade, AirStrike };

	public Command[] Pool;

	private int CurrentCommandIndex;

	public Command PopNextCommand()
	{
		Command result = Pool[CurrentCommandIndex];

		CurrentCommandIndex++;
		CurrentCommandIndex %= Pool.Length;

		return result;
	}
}
