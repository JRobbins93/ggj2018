﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CodeDifficultyManager : MonoBehaviour {

	public class CodeDifficultyAdvancedEvent : UnityEvent<CodeDifficultyBracket> {}

	private class CodeDifficultyBracketComparer : IComparer<CodeDifficultyBracket>
	{
		public int Compare(CodeDifficultyBracket a, CodeDifficultyBracket b)
		{
			return a.StartTime.CompareTo(b.StartTime);
		}
	}

	[System.Serializable]
	public class CodeDifficultyBracket
	{
		public int MaximumWordLength;
		public int MinimumWordLength;
		public int MaximumNumberOfWords;
		public int MinimumNumberOfWords;

		public float CodeScrollSpeed;

		public float StartTime;
	}

	private static CodeDifficultyAdvancedEvent CodeDifficultyAdvanced = new CodeDifficultyAdvancedEvent();

	public CodeDifficultyBracket[] DifficultyBrackets;

	private int CurrentCodeDifficultyBracketIndex;
	private float TimeToAdvanceDifficultyBracket;

	public static void AddCodeDifficultyAdvancedListener(UnityAction<CodeDifficultyBracket> callback)
	{
		CodeDifficultyAdvanced.AddListener(callback);
	}

	public static void RemoveCodeDifficultyAdvancedListener(UnityAction<CodeDifficultyBracket> callback)
	{
		CodeDifficultyAdvanced.RemoveListener(callback);
	}

	private void Awake()
	{
		CodeDifficultyBracketComparer comparer = new CodeDifficultyBracketComparer();
		System.Array.Sort(DifficultyBrackets, comparer);

		CurrentCodeDifficultyBracketIndex = 0;

		TimeToAdvanceDifficultyBracket = DifficultyBrackets[1].StartTime;
	}

	private void OnEnable()
	{
		CodeDifficultyAdvanced.Invoke(DifficultyBrackets[0]);
	}

	private void Update()
	{
		if (CurrentCodeDifficultyBracketIndex == DifficultyBrackets.Length - 1)
		{
			return;
		}

		if (Time.timeSinceLevelLoad >= TimeToAdvanceDifficultyBracket)
		{
			AdvanceDifficultyBracket();
		}
	}

	private void AdvanceDifficultyBracket()
	{
		CurrentCodeDifficultyBracketIndex++;

		CodeDifficultyBracket nextBracket = DifficultyBrackets[CurrentCodeDifficultyBracketIndex];

		TimeToAdvanceDifficultyBracket = nextBracket.StartTime;

		CodeDifficultyAdvanced.Invoke(nextBracket);
	}
}
