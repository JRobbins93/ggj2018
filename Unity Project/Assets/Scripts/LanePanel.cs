﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LanePanel : MonoBehaviour {

	public Transform[] Lanes;

	public UnityEngine.UI.Text[] TurretHealthTexts;

	public Turret[] Turrets;

	public UnityEngine.UI.Text PendingActionText;

	public UnityEngine.UI.Text SelectLaneText;

	private UnityAction<int> PendingAction;

	public GameObject GameOverText;

	public GameObject CommandCodePanelObject;

	private void Start()
	{
		CommandCodePanel.AddShootListener(EnqueueFireTurret);
		CommandCodePanel.AddUpgradeListener(EnqueueUpgradeTurret);
		CommandCodePanel.AddRepairListener(EnqueueRepairTurret);
		CommandCodePanel.AddAirStrikeListener(AirStrike);

		KeyboardInput.AddNumericalKeyPressedListener(TriggerQueuedAction);

		SelectLaneText.gameObject.SetActive(false);
	}

	private void Update()
	{
		for (int laneIndex = 0; laneIndex < Lanes.Length; laneIndex++)
		{
			Enemy[] enemiesInLane = Lanes[laneIndex].GetComponentsInChildren<Enemy>();

			foreach (Enemy enemy in enemiesInLane)
			{
				if (!enemy.IsDead() && enemy.HasStoppedMoving())
				{
					Turrets[laneIndex].ApplyDamage(enemy.DamagePerSecond * Time.deltaTime);

					if (Turrets[laneIndex].IsDead())
					{
						Time.timeScale = 0.0f;
						Turrets[laneIndex].BarrierMeshObject.gameObject.SetActive(false);
						CommandCodePanelObject.SetActive(false);
						GameOverText.SetActive(true);
					}
				}
			}
		}

		RefreshHealthReadout();
	}

	public void OnGameOverClicked()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
	}

	private void RefreshHealthReadout()
	{
		for (int turretIndex = 0; turretIndex < Turrets.Length; turretIndex++)
		{
			TurretHealthTexts[turretIndex].text = string.Format("{0}: {1}HP", turretIndex + 1, Mathf.Max(0f, (int)Turrets[turretIndex].GetHealth()));
		}
	}

	private void EnqueueFireTurret()
	{
		PendingAction = FireTurret;
		PendingActionText.text = "SHOOT";
		SelectLaneText.gameObject.SetActive(true);
	}

	private void EnqueueRepairTurret()
	{
		PendingAction = RepairTurret;
		PendingActionText.text = "REPAIR";
		SelectLaneText.gameObject.SetActive(true);
	}

	private void EnqueueUpgradeTurret()
	{
		PendingAction = UpgradeTurret;
		PendingActionText.text = "UPGRADE";
		SelectLaneText.gameObject.SetActive(true);
	}

	private void TriggerQueuedAction(int lane)
	{
		if (lane >= Lanes.Length)
		{
			return;
		}

		if (PendingAction == null)
		{
			return;
		}

		PendingAction(lane);

		PendingAction = null;
		PendingActionText.text = string.Empty;
		SelectLaneText.gameObject.SetActive(false);
	}

	private void FireTurret(int lane)
	{
		Turret turretToFire = Turrets[lane];

		int shotsToFire = turretToFire.GetShotsPerAttack();

		Enemy enemyToDamage = GetNearestEnemyInLane(lane);

		while (shotsToFire > 0)
		{
			if (enemyToDamage == null)
			{
				break;
			}
			else
			{
				enemyToDamage.ApplyDamage(turretToFire.GetDamagePerShot());

				if (enemyToDamage.IsDead())
				{
					enemyToDamage = GetNearestEnemyInLane(lane);
				}
			}

			shotsToFire--;
		}
	}

	private void UpgradeTurret(int lane)
	{
		Turrets[lane].Upgrade();
	}

	private void RepairTurret(int lane)
	{
		Turrets[lane].Repair();
	}

	private void AirStrike()
	{
		for (int laneIndex = 0; laneIndex < Lanes.Length; laneIndex++)
		{
			Enemy nearestEnemy = GetNearestEnemyInLane(laneIndex);

			while (nearestEnemy != null)
			{
				nearestEnemy.Kill();
				nearestEnemy = GetNearestEnemyInLane(laneIndex);
			}
		}
	}

	private Enemy GetNearestEnemyInLane(int laneIndex)
	{
		Enemy nearestEnemy = null;

		Transform lane = Lanes[laneIndex];
		Enemy[] enemies = lane.GetComponentsInChildren<Enemy>();

		foreach (Enemy enemy in enemies)
		{
			if (!enemy.IsDead() && (nearestEnemy == null || enemy.transform.position.x > nearestEnemy.transform.position.x))
			{
				nearestEnemy = enemy;
			}
		}

		return nearestEnemy;
	}
}
