﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class WordDictionary : MonoBehaviour {

    private static string[][] Words;

    private const string TEXT_FILE_FOLDER = "Words";
    private const string SPLIT_BY_LINES_REGEX = @"\r?\n|\r";

    private const int SHORTEST_WORD_COUNT = 3;
	private const int LONGEST_WORD_COUNT = 10;

	private static StringBuilder StringBuilder = new StringBuilder();

	public static string GetRandomCode(int minimumNumberOfWords, int maximumNumberOfWords, int minimumWordLength, int maximumWordLength)
	{
		StringBuilder.Remove(0, StringBuilder.Length);

		int numberOfWords = Random.Range(minimumNumberOfWords, maximumNumberOfWords);

		for (int wordIndex = 0; wordIndex < numberOfWords; wordIndex++)
		{
			int lengthOfWord = Random.Range(minimumWordLength, maximumWordLength);

			string word = GetRandomWord(lengthOfWord);

			if (wordIndex != numberOfWords - 1)
			{
				StringBuilder.AppendFormat("{0} ", word);
			}
			else
			{
				StringBuilder.Append(word);
			}
		}

		return StringBuilder.ToString();
	}

	public static string GetRandomWord(int length)
	{
		int dictionaryIndex = length - SHORTEST_WORD_COUNT;

		string[] wordsOfLength = Words[dictionaryIndex];

		int randomIndex = Random.Range(0, wordsOfLength.Length - 1);

		return Words[dictionaryIndex][randomIndex];
	}

    private static void ImportWords()
    {
        Object[] textFiles = Resources.LoadAll(TEXT_FILE_FOLDER);

        int numberOfTextFiles = textFiles.Length;

        Words = new string[numberOfTextFiles][];

        for(int textFilesIndex = 0; textFilesIndex < numberOfTextFiles; textFilesIndex++)
        {
            TextAsset textFile = (TextAsset)textFiles[textFilesIndex];

            string contents = textFile.text;

            string[] lines = Regex.Split(contents, SPLIT_BY_LINES_REGEX);

			Words[textFilesIndex] = lines;
        }
    }

    private void Awake()
    {
        ImportWords();
    }
}
