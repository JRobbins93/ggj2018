﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	private class EnemySpawnPhaseComparer : IComparer<EnemySpawnPhase>
	{
		public int Compare(EnemySpawnPhase a, EnemySpawnPhase b)
		{
			return a.StartTime.CompareTo(b.StartTime);
		}
	}

	[System.Serializable]
	public class EnemySpawnPhase
	{
		public int[] SpawnProbabilities = new int[3];

		public float StartTime;

		public int GetEnemyToSpawn()
		{
			int totalProbability = 0;

			foreach (int probability in SpawnProbabilities)
			{
				totalProbability += probability;
			}

			int random = Random.Range(0, totalProbability);

			int total = 0;

			for(int probabilityIndex = 0; probabilityIndex < SpawnProbabilities.Length; probabilityIndex++)
			{
				if (total + SpawnProbabilities[probabilityIndex] >= random)
				{
					return probabilityIndex;
				}
			}

			return SpawnProbabilities.Length - 1;
		}
	}

	public EnemySpawnPhase[] SpawnPhases;

	public float SpawnInterval;

	public Transform[] Lanes;

	public GameObject[] EnemyPrefabs;

	private Enemy[] Enemies;

	private float SpawnTimer;

	private int CurrentSpawnPhaseIndex;

	private float TimeToAdvanceSpawnPhase;

	private void Awake()
	{
		Enemies = new Enemy[EnemyPrefabs.Length];

		for (int enemyIndex = 0; enemyIndex < EnemyPrefabs.Length; enemyIndex++)
		{
			Enemies[enemyIndex] = EnemyPrefabs[enemyIndex].GetComponent<Enemy>();
		}

		System.Array.Sort(SpawnPhases, new EnemySpawnPhaseComparer());

		TimeToAdvanceSpawnPhase = SpawnPhases[1].StartTime;
	}

	private void Update()
	{
		SpawnTimer += Time.deltaTime;

		if (SpawnTimer >= SpawnInterval)
		{
			SpawnTimer %= SpawnInterval;
			SpawnEnemy();
		}

		if (CurrentSpawnPhaseIndex == SpawnPhases.Length - 1 && Time.timeSinceLevelLoad >= TimeToAdvanceSpawnPhase)
		{
			AdvanceSpawnPhase();
		}
	}

	private void SpawnEnemy()
	{
		Debug.Log("Spawning");
		int enemyIndexToSpawn = SpawnPhases[CurrentSpawnPhaseIndex].GetEnemyToSpawn();

		int laneIndex = Random.Range(0, Lanes.Length);

		GameObject.Instantiate(EnemyPrefabs[enemyIndexToSpawn], Lanes[laneIndex]);
	}

	private void AdvanceSpawnPhase()
	{
		CurrentSpawnPhaseIndex++;

		EnemySpawnPhase nextSpawnPhase = SpawnPhases[CurrentSpawnPhaseIndex];

		TimeToAdvanceSpawnPhase = nextSpawnPhase.StartTime;
	}
}
