﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private const float MAXIMUM_X_POSITION = 31f;

	public float MoveSpeed;

	public float DamagePerSecond;

	public float StartingHealth;

	public int Value;

	private RectTransform RectTransform;

	private float CurrentHealth;

	public void ApplyDamage(float damage)
	{
		CurrentHealth -= damage;

		if (IsDead())
		{
			Kill();
		}
	}

	public void Kill()
	{
		CurrentHealth = 0f;
		Destroy(gameObject);
	}

	public bool IsDead()
	{
		return CurrentHealth <= 0f;
	}

	private void Awake()
	{
		RectTransform = GetComponent<RectTransform>();

		CurrentHealth = StartingHealth;

		transform.localPosition = new Vector2(0f, transform.position.y);
	}

	private void Update()
	{
		if (transform.position.x < MAXIMUM_X_POSITION)
		{
			float newXPosition = Mathf.MoveTowards(transform.localPosition.x, MAXIMUM_X_POSITION, MoveSpeed * Time.deltaTime);
			transform.localPosition = new Vector2(newXPosition, transform.position.y);
		}
	}

	public bool HasStoppedMoving()
	{
		return transform.localPosition.x == MAXIMUM_X_POSITION;
	}
}
