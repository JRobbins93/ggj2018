﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CommandCodePanel : MonoBehaviour {

	private static UnityEvent ShootEvent = new UnityEvent();
	private static UnityEvent UpgradeEvent = new UnityEvent();
	private static UnityEvent RepairEvent = new UnityEvent();
	private static UnityEvent AirStrikeEvent = new UnityEvent();

	public static void AddShootListener(UnityAction callback)
	{
		ShootEvent.AddListener(callback);
	}

	public static void AddUpgradeListener(UnityAction callback)
	{
		UpgradeEvent.AddListener(callback);
	}

	public static void AddRepairListener(UnityAction callback)
	{
		RepairEvent.AddListener(callback);
	}

	public static void AddAirStrikeListener(UnityAction callback)
	{
		AirStrikeEvent.AddListener(callback);
	}

	public GameObject EntryPrefab;

	public CommandPool CommandPool;

	private const float ENTRY_DELETE_Y_POSITION = 128.125f;
	private const float ENTRY_HEIGHT = 51.25f;

	private const int ENTRIES_PER_SCREEN =5;

	private Queue<CommandCodeEntry> Entries;

	private float EntryMovementSpeed;

	private CodeDifficultyManager.CodeDifficultyBracket CurrentDifficultyBracket;

	private UnityAction<string> SelectedAction;

	private void Awake()
	{
		Entries = new Queue<CommandCodeEntry>();

		CodeDifficultyManager.AddCodeDifficultyAdvancedListener(OnDifficultyAdvanced);
	}

	private void Start()
	{
		CreateCommandInstructionEntry(CommandPool.PopNextCommand());
	}

	private void Update()
	{
		foreach (CommandCodeEntry entry in Entries)
		{
			entry.GetRectTransform().anchoredPosition += Vector2.up * EntryMovementSpeed * Time.deltaTime;
		}

		CommandCodeEntry uppermostEntry = Entries.Peek();

		if(Entries.Count < ENTRIES_PER_SCREEN)
		{
			if (uppermostEntry.GetRectTransform().anchoredPosition.y >= -128.5f + ENTRY_HEIGHT * Entries.Count)
			{
				CreateCommandInstructionEntry(CommandPool.PopNextCommand());
			}
		}

		if(HasEntryVanished(uppermostEntry))
		{
			RemoveUppermostEntry();
		}
	}

	private void CreateCommandInstructionEntry(CommandPool.Command command)
	{
		GameObject entryObject = GameObject.Instantiate(EntryPrefab, transform);

		entryObject.GetComponent<RectTransform>().anchoredPosition3D = Vector3.up * -128.125f;

		CommandCodeEntry entry = entryObject.GetComponent<CommandCodeEntry>();

		string code = WordDictionary.GetRandomCode(CurrentDifficultyBracket.MinimumNumberOfWords, CurrentDifficultyBracket.MaximumNumberOfWords, CurrentDifficultyBracket.MinimumWordLength, CurrentDifficultyBracket.MaximumWordLength);

		entry.Init(command.ToString(), code);

		Entries.Enqueue(entry);

		CodeWatcher.AddCallbackForCode(code, OnCodeEntered);

		UnityAction<string> callback = null;

		switch (command)
		{
		case CommandPool.Command.Shoot:
			callback = OnShootTriggered;
			break;
		case CommandPool.Command.Upgrade:
			callback = OnUpgradeTriggered;
			break;
		case CommandPool.Command.Repair:
			callback = OnRepairTriggered;
			break;
		case CommandPool.Command.AirStrike:
			callback = OnAirstrikeTriggered;
			break;
		}

		CodeWatcher.AddCallbackForCode(code, callback);
	}

	private void OnCodeEntered(string code)
	{
		foreach (CommandCodeEntry entry in Entries)
		{
			if (entry.GetCode().ToLower().Equals(code.ToLower()))
			{
				entry.gameObject.SetActive(false);
			}
		}
	}

	private void OnShootTriggered(string code)
	{
		ShootEvent.Invoke();
	}

	private void OnUpgradeTriggered(string code)
	{
		UpgradeEvent.Invoke();
	}

	private void OnRepairTriggered(string code)
	{
		RepairEvent.Invoke();
	}

	private void OnAirstrikeTriggered(string code)
	{
		AirStrikeEvent.Invoke();
	}

	private void RemoveUppermostEntry()
	{
		CommandCodeEntry uppermostEntry = Entries.Dequeue();
		GameObject.Destroy(uppermostEntry.gameObject);

		CodeWatcher.RemoveCallbacksForCode(uppermostEntry.GetCode());
	}

	private bool HasEntryVanished(CommandCodeEntry entry)
	{
		return entry.GetRectTransform().anchoredPosition.y >= ENTRY_DELETE_Y_POSITION;
	}

	private void OnDifficultyAdvanced(CodeDifficultyManager.CodeDifficultyBracket bracket)
	{
		CurrentDifficultyBracket = bracket;

		EntryMovementSpeed = bracket.CodeScrollSpeed;
	}
}
